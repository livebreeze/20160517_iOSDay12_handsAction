//
//  ViewController.swift
//  20160517_iOSDay12_handsAction
//
//  Created by ChenSean on 5/17/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var targetConstraint: NSLayoutConstraint? = nil
    
    @IBAction func OnClick(sender: AnyObject) {
        guard targetConstraint != nil else {
            return
        }
        
        if targetConstraint?.constant == 0 {
            targetConstraint?.constant = -240
        } else {
            targetConstraint?.constant = 0
        }
        
        UIView.animateWithDuration(0.5) { 
            self.view.layoutIfNeeded()
        }
    }

    @IBAction func MyScreenEdgePanGestrueAction(sender: UIScreenEdgePanGestureRecognizer) {
        let point = sender.locationInView(sender.view)
        print("x = \(point.x), y = \(point.y)")
        
        if targetConstraint?.constant == 0 {
            targetConstraint?.constant = -240
        } else {
            targetConstraint?.constant = 0
        }
        
        UIView.animateWithDuration(0.5) {
            self.view.layoutIfNeeded()
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        for constraint in view.constraints{
            if constraint.identifier == "abc" {
                targetConstraint = constraint
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

